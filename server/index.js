const express = require('express')
const app = express()
const cors = require('cors')
const port = 3000
const fetch = require('node-fetch')

app.use(cors())
app.use(express.json())

app.post('/authorize-paypal-order', (req, res) => {
    console.log(req.body)
    var orderIdCapture = req.body.orderID
    fetch(`https://api-m.sandbox.paypal.com/v2/checkout/orders/${orderIdCapture}/capture`,
    {
        method: 'post',
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic QVpIazNRczdBZEhudWs3YWhubmxLYnRIOXhyNGdZNDF4cS1WbGVIQ1hnSzN2TzVKVWpEVHhNVWctcmV4dVppMVRoaGJ4SWItdmhHS1g1RTI6RUhBWDIzaGZyYm9hNWtkU3h2R0MzXzZlb05SdW1PdjdvN3hrN2ZfdnEwNzJNMkw1SEsyRjlYMGVoZzloWnh1bzZZdHFKcWg5ZVlWejdaZ0c="
        },
    }).then(data => {
        console.log(data)
        res.send('Hello World!')
    }).catch(data => {
        console.log(data)
    })
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
