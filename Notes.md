# Diagram
![Alt text](Screenshot_20230325_175350.png)

# Sequence
1) create order on client side\
<span style="color:lightgreen"> v2/checkout/orders </span>

2) approve order on server side (postman)\
<span style="color:lightgreen">/v2/checkout/orders/:order_id/authorize </span>

3) capture order on server side (postman)\
<span style="color:lightgreen"> /v2/checkout/orders/:order_id/capture </span>

4) The payer's card is charged.\

# Commands
```shell
http-server # Client
npm start   # Server
curl "http://localhost:3000/capture?orderId=<Order Id>" # Execute payment capture
```
# Customer login details
- Email - sb-4jtec25325190@personal.example.com
- Password - 3wy}8$^D

client - create-order *
client - approvel
server - authorize - paypal
shipping
server - capture - paypal

# Important Doc
https://developer.paypal.com/docs/checkout/standard/customize/authorization/

# Paypal Application Details
Client ID - AZHk3Qs7AdHnuk7ahnnlKbtH9xr4gY41xq-VleHCXgK3vO5JUjDTxMUg-rexuZi1ThhbxIb-vhGKX5E2
Secret - EHAX23hfrboa5kdSxvGC3_6eoNRumOv7o7xk7f_vq072M2L5HK2F9X0ehg9hZxuo6YtqJqh9eYVz7ZgG
